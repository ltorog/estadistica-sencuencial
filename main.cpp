/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Ltoro
 *
 * Created on 30 de noviembre de 2018, 18:41
 */

#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <fstream>
#include <Math.h>

#include "Funciones.h"

using namespace std;

int main(int argc, char** argv){
    
    ifstream Texto("numeros.txt");
    string linea;
    float Nota;
    
    double long SumaNotas=0;
    int NumeroNotas=0;
    
    if(Texto.is_open()){
        while(getline(Texto,linea)){
            NumeroNotas++;
            linea.replace(linea.begin(),linea.end(),',','.');
            Nota=strtof((linea).c_str(),0);
            SumaNotas+=Nota;
        }
        
        double long Prom=Promedio(SumaNotas,NumeroNotas);
        
        cout << "La suma total es: " << SumaNotas << endl;
        cout << "La cantidad es datos es: " << NumeroNotas << endl;
        cout << "La media es: " << Prom << endl;
        
        float VarianzaMuestral=0;
        while(getline(Texto,linea)){
            linea.replace(linea.begin(),linea.end(),',','.');
            Nota=strtof((linea).c_str(),0);
            VarianzaMuestral+=pow(Nota-Prom,2);
        }
        
        VarianzaMuestral/=(NumeroNotas-1);
        
        float DesviacionEstandar = sqrt(VarianzaMuestral);
        
        cout << "La desviacion estandar es: " << DesviacionEstandar << endl;
        
        Texto.close();
        return 1;
    }
    else{
        cout<< "No existe el archivo" << endl;
        return 1;
    }
    return 0;
}

